import * as React from "react";

import * as AwsS3 from "@uppy/aws-s3";
import * as Uppy from "@uppy/core";
import { DashboardModal } from "@uppy/react";

import "@uppy/core/dist/style.css";
import "@uppy/dashboard/dist/style.css";

export interface FileUploadProps {
    onChange?: (url: string) => void;
    uploadPath?: (fieldName: string) => void;
    limit?: number;
    timeout?: number;
    onClick?: (e: any) => void;
    allowedFileTypes?: string[];
}

interface FileUploadState {
    visible: boolean;
}

export class FileUpload extends React.Component<FileUploadProps, FileUploadState> {
    private uppy: Uppy.Uppy;

    state = {
        visible: false
    };

    constructor(props: FileUploadProps) {
        super(props);

        this.uppy = Uppy({ autoProceed: true, debug: process.env.NODE_ENV !== "production" }).use(AwsS3, {
            limit: props.limit || 1,
            timeout: props.timeout || 120000,
            companionUrl: process.env.NODE_ENV !== "production" ? "http://localhost:4000" : process.env.BACKEND_URL,
            restrictions: {
                allowedFileTypes: props.allowedFileTypes || []
            }
        });

        this.uppy.on("complete", result => {
            const url = result.successful[0].uploadURL;
            if (props.onChange) {
                props.onChange(url);
            }
        });
    }

    public render() {
        const { onChange, uploadPath, limit = 1, timeout = 120000, onClick, ...buttonProps } = this.props;

        return (
            <>
                <button
                    {...buttonProps}
                    onClick={e => {
                        this.setState({
                            visible: true
                        });

                        if (onClick) {
                            onClick(e);
                        }
                    }}
                />
                {process.browser && (
                    <DashboardModal
                        uppy={this.uppy}
                        open={this.state.visible}
                        target={document.body}
                        onRequestClose={() => {
                            this.setState({
                                visible: false
                            });
                        }}
                    />
                )}
            </>
        );
    }
}
