declare namespace NodeJS {
    interface Process {
        browser: boolean;
    }
}

declare module "*.gql" {
    const content: any;
    export default content;
}
