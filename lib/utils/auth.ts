import cookie from "cookie";
import { ApolloClient } from "apollo-boost";
import { CurrentUserDocument } from "../generated/graphqlgen";

export const setToken = (token: string, timeInSeconds: number = 30 * 24 * 60 * 60) => {
    document.cookie = cookie.serialize("token", token, {
        maxAge: timeInSeconds, // 30 days
        path: "/",
        domain: null
    });
};

export const checkLoggedIn = async (apolloClient: ApolloClient<any>) => {
    const result = await apolloClient.query({
        query: CurrentUserDocument
    });

    return result.data.currentUser;
};
