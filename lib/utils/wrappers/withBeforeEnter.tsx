import * as React from "react";
import { NextContext } from "next";

export type BeforeEnterProps = {
    user: any;
};

export type BeforeEnterFunc = (ctx: NextContext, props: BeforeEnterProps) => Promise<any>;

export const withBeforeEnter = (fn: BeforeEnterFunc) => App =>
    class WithBeforeEnter extends React.Component {
        public static async getInitialProps(ctx) {
            let otherProps = {};
            if (App.getInitialProps != null) {
                otherProps = await App.getInitialProps(ctx);
            }

            const additionalProps = await fn(ctx, otherProps as any);

            return {
                ...otherProps,
                ...additionalProps
            };
        }
    };
