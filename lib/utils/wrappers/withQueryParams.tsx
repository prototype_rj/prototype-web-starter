import * as React from "react";

export const withQueryParams = App =>
    class WithQueryParams extends React.Component {
        public static async getInitialProps(ctx) {
            let otherProps = {};
            if (App.getInitialProps != null) {
                otherProps = await App.getInitialProps(ctx);
            }

            return {
                queryParams: ctx.query,
                ...otherProps
            };
        }

        public render() {
            return <App {...this.props} />;
        }
    };
