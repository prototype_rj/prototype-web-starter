import * as React from "react";
import { checkLoggedIn } from "../auth";

export const withUser = App =>
    class WithUser extends React.Component {
        public static async getInitialProps(ctx) {
            let otherProps = {};
            if (App.getInitialProps != null) {
                otherProps = await App.getInitialProps(ctx);
            }

            try {
                const currentUser = await checkLoggedIn(ctx.apolloClient);

                return {
                    ...otherProps,
                    currentUser
                };
            } catch (e) {
                return {
                    ...otherProps,
                    currentUser: null
                };
            }
        }

        public render() {
            return <App {...this.props} />;
        }
    };
