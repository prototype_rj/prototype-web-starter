import { withUser } from "./withUser";
import { withQueryParams } from "./withQueryParams";

export const PageWrapper = Page => withUser(withQueryParams(Page));
