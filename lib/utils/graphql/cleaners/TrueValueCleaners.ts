// Recusrively replaces all key value pairs in obj of value {[trueValueKey]: VALUE} with VALUE
export const manifestTrueValue = (trueValueKey: string, obj: object): object => {
    let ret = obj;
    for (const key of Object.keys(obj)) {
        if (key === trueValueKey) {
            ret = obj[trueValueKey];
        }

        const isObj = val => typeof val === "object" && val;
        if (isObj(obj[key])) {
            obj[key] = manifestTrueValue(trueValueKey, obj[key]);
        } else if (Array.isArray(obj[key])) {
            for (let i = 0; i < obj[key].length; i++) {
                if (isObj(obj[key][i])) {
                    obj[key][i] = manifestTrueValue(trueValueKey, obj[key][i]);
                }
            }
        }
    }

    return ret;
};
