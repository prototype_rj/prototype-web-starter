import cookie from "cookie";

export const parseCookies = (req, options = {}) =>
    cookie.parse(req ? req.headers.cookie || "" : document.cookie, options);

export const getToken = (req?: any) => {
    if (process.browser) {
        return cookie.parse(document.cookie).token;
    }

    return parseCookies(req).token;
};
