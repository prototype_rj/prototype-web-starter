import { ApolloClient } from "apollo-boost";

export interface WithApolloClientProps {
    apolloClient: ApolloClient<any>;
}
