const withTypescript = require("@zeit/next-typescript");
const withCss = require("@zeit/next-css");
const webpack = require("webpack");

module.exports = withCss(
    withTypescript({
        target: "serverless",
        webpack(config, options) {
            config.module.rules = [
                ...config.module.rules,
                {
                    test: /\.(graphql|gql)$/,
                    exclude: /node_modules/,
                    loader: "graphql-tag/loader"
                }
            ];

            config.plugins.push(
                new webpack.EnvironmentPlugin({
                    NODE_ENV: "development",
                    BACKEND_URL: "{{GRAPHQL_HTTP_BACKEND_URL}}"
                })
            );

            config.optimization.minimizer = [];

            return config;
        }
    })
);
