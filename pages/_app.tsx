import App, { Container } from "next/app";
import Head from "next/head";
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from "apollo-boost";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";

import withApolloClient from "../lib/utils/graphql/withApolloClient";

class MyApp extends App<{ apolloClient: ApolloClient<any> }> {
    static async getInitialProps({ Component, ctx }) {
        let pageProps = {};

        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }

        return { pageProps };
    }

    render() {
        const { Component, pageProps, apolloClient } = this.props;

        return (
            <Container>
                <Head>
                    <title>PrototypeProject</title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                    <link rel="stylesheet" type="text/css" href="https://useprototype.com/static/page.css" />
                    {/* {{ ADDITIONAL_FONTS }} */}
                </Head>
                <ApolloProvider client={apolloClient}>
                    <ApolloHooksProvider client={apolloClient}>
                        <Component {...pageProps} />
                    </ApolloHooksProvider>
                </ApolloProvider>
            </Container>
        );
    }
}

export default withApolloClient(MyApp);
